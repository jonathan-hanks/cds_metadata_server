package main

import (
	"encoding/json"
	"flag"
	"git.ligo.org/jonathan-hanks/cds_metadata_server/middleware"
	"git.ligo.org/jonathan-hanks/cds_metadata_server/parser"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

type DcuHistory struct {
	Id      int
	History []parser.DcuConfig
	Lock    sync.Mutex
}

func NewDcuHistory(id int) *DcuHistory {
	return &DcuHistory{Id: id, History: make([]parser.DcuConfig, 0, 0)}
}

func (dh *DcuHistory) AddConfig(config parser.DcuConfig) {
	const MaxLength = 5

	if config.DcuId != dh.Id {
		return
	}
	dh.Lock.Lock()
	defer dh.Lock.Unlock()

	if len(dh.History) > 0 && dh.History[0].ConfigHash == config.ConfigHash {
		return
	}

	historyLength := len(dh.History)
	if historyLength >= MaxLength {
		historyLength = MaxLength - 1
	}

	dh.History = append([]parser.DcuConfig{config}, dh.History[0:historyLength]...)
}

func (dh *DcuHistory) AvailableConfigs() []uint32 {
	dh.Lock.Lock()
	defer dh.Lock.Unlock()

	available := make([]uint32, len(dh.History))
	for i := range dh.History {
		available[i] = dh.History[i].ConfigHash
	}
	return available
}

func (dh *DcuHistory) Config(configHash uint32) (parser.DcuConfig, bool) {
	dh.Lock.Lock()
	defer dh.Lock.Unlock()

	for i := range dh.History {
		if dh.History[i].ConfigHash == configHash {
			return dh.History[i], true
		}
	}
	return parser.DcuConfig{}, false
}

type IfoHistory struct {
	Dcus map[int]*DcuHistory
	Lock sync.RWMutex
}

func NewIfoHistory() *IfoHistory {
	return &IfoHistory{Dcus: make(map[int]*DcuHistory, 255)}
}

func (ih *IfoHistory) AddIfoConfig(config *parser.IfoConfig) {
	ih.Lock.Lock()
	defer ih.Lock.Unlock()

	for dcuId, dcuConfig := range config.Dcus {
		history, ok := ih.Dcus[dcuId]
		if !ok {
			history = NewDcuHistory(dcuId)
		}
		history.AddConfig(dcuConfig)
		ih.Dcus[dcuId] = history
	}
}

func (ih *IfoHistory) DcuHistory(dcuId int) (history *DcuHistory, ok bool) {
	ih.Lock.RLock()
	defer ih.Lock.RUnlock()

	history, ok = ih.Dcus[dcuId]
	return
}

type ConfigOpts struct {
	ListenAddress string
	MasterPath    string
}

func parseArgs() ConfigOpts {

	address := flag.String("listen", ":9010", "Interface and port to listen on")
	master := flag.String("master", "master", "Path to the master ini file")
	flag.Parse()
	return ConfigOpts{
		ListenAddress: *address,
		MasterPath:    *master,
	}
}

func handleError(w http.ResponseWriter, statusCode int, msg string) {
	type ErrorMsg struct {
		ErrorMessage string
	}
	output := &ErrorMsg{ErrorMessage: msg}
	w.WriteHeader(statusCode)
	data, _ := json.Marshal(output)
	_, _ = w.Write(data)
}

func getDcuId(r *http.Request) (int, error) {
	params := middleware.ReMuxParams(r)
	val, err := strconv.ParseInt(params["dcuid"], 10, 32)
	if err != nil {
		return 0, err
	}
	return int(val), nil
}

func getConfigHash(r *http.Request) (uint32, error) {
	params := middleware.ReMuxParams(r)
	val, err := strconv.ParseUint(params["confighash"], 10, 32)
	if err != nil {
		return 0, err
	}
	return uint32(val), nil
}

func indexHandler(w http.ResponseWriter, r *http.Request, ifoConfig *parser.IfoConfig) {
	w.Header().Set("content-type", "application/json")
	encoder := json.NewEncoder(w)
	encoder.Encode(ifoConfig)
}

func dcuHandler(w http.ResponseWriter, r *http.Request, ifoHistory *IfoHistory) {
	dcuId, err := getDcuId(r)
	if err != nil {
		handleError(w, 404, "Unkown or bad dcuid")
		return
	}
	dcuHistory, ok := ifoHistory.DcuHistory(dcuId)
	if !ok {
		handleError(w, 404, "dcu not found")
		return
	}
	type AvailableConfig struct {
		DcuId           int
		AvailableConfig []uint32
	}

	configs := &AvailableConfig{
		DcuId:           dcuId,
		AvailableConfig: dcuHistory.AvailableConfigs(),
	}
	out, _ := json.Marshal(configs)
	_, _ = w.Write(out)
}

func dcuConfigHandler(w http.ResponseWriter, r *http.Request, ifoHistory *IfoHistory) {
	dcuId, err := getDcuId(r)
	if err != nil {
		handleError(w, 404, "Unknown or bad dcuid")
		return
	}
	configHash, err := getConfigHash(r)
	if err != nil {
		handleError(w, 404, "Unknown or bad config hash")
		return
	}
	dcuHistory, ok := ifoHistory.DcuHistory(dcuId)
	if !ok {
		handleError(w, 404, "dcu not found")
		return
	}

	config, ok := dcuHistory.Config(configHash)
	if !ok {
		handleError(w, 404, "config not found")
		return
	}

	out, _ := json.Marshal(&config)
	_, _ = w.Write(out)
}

func main() {
	cfg := parseArgs()

	cfgLock := sync.Mutex{}
	ifoConfig, err := parser.Parse(cfg.MasterPath)
	if err != nil {
		log.Println("Error in parsing files:", err)
	}

	ifoHistory := NewIfoHistory()
	ifoHistory.AddIfoConfig(ifoConfig)

	getConfig := func() *parser.IfoConfig {
		cfgLock.Lock()
		defer cfgLock.Unlock()
		return ifoConfig
	}
	getHistory := func() *IfoHistory {
		cfgLock.Lock()
		defer cfgLock.Unlock()
		return ifoHistory
	}
	setConfig := func(cfg *parser.IfoConfig) {
		cfgLock.Lock()
		defer cfgLock.Unlock()
		ifoConfig = cfg
		ifoHistory.AddIfoConfig(cfg)
	}

	_ = getConfig()
	_ = setConfig

	quitChannel := make(chan bool)

	go updateLoop(cfg.MasterPath, ifoConfig.Files, setConfig, quitChannel)

	middle := middleware.Chain(middleware.LoggingHandler)
	mux := middleware.NewReMux()

	mux.HandleFunc("/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		indexHandler(w, r, getConfig())
	}))
	mux.HandleFunc("/(?P<dcuid>[0-9]+)/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		dcuHandler(w, r, getHistory())
	}))
	mux.HandleFunc("/(?P<dcuid>[0-9]+)/(?P<confighash>[0-9]+)/", middle.Apply(func(w http.ResponseWriter, r *http.Request) {
		dcuConfigHandler(w, r, getHistory())
	}))

	log.Println("Listening on ", cfg.ListenAddress)
	err = http.ListenAndServe(cfg.ListenAddress, mux)
	quitChannel <- true
	log.Fatal(err)
}

func updateLoop(masterPath string, files []parser.ConfigFileInfo, setConfig func(config *parser.IfoConfig), quitChannel chan bool) {
	const Duration = 5 * time.Second
	timer := time.NewTimer(Duration)
	defer func() {
		if !timer.Stop() {
			<-timer.C
		}
	}()
	for {
		select {
		case <-quitChannel:
			return
		case <-timer.C:
			reparse := false
			for _, entry := range files {
				fInfo, err := os.Stat(entry.Path)
				if err != nil {
					continue
				}
				if fInfo.ModTime().Unix() > entry.Timestamp.Unix() {
					log.Printf("Change detected in '%s', will reparse config", entry.Path)
					reparse = true
					break
				}
			}
			if reparse {

				cfg, err := parser.Parse(masterPath)
				if err == nil {
					log.Printf("Successfully parsed '%s'", masterPath)
					files = cfg.Files
					setConfig(cfg)

				} else {
					log.Printf("Error parsing '%s' %v", masterPath, err)
				}
			}
			timer.Reset(Duration)
		}
	}
}
