A prototype service for exporting metadata from CDS.

This reads the ini files and reflects it out via http.   The process is currently only designed for prototyping and experimentation.

This is designed and tested against Go 1.14.

To build:

go build .

To run:

cds_metadata_server -master /opt/rtcds/SITE/IFO/daq0/running/master

It will by default open 0.0.0.0:9010 and server http.