package parser

// #include <stdlib.h>
// #include "daq_data_types.h"
// #include "daqmap.h"
// #include "param.h"
// extern int parseCbBridge(char*, CHAN_PARAM*, void*);
// extern int parse_config_file(char* fname, unsigned long* crc,
//                       int testpoint, void* user);
import "C"

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"git.ligo.org/jonathan-hanks/cds_metadata_server/ligo_crc"
	"hash"
	"os"
	"strings"
	"sync"
	"time"
	"unsafe"
)

const (
	MaxDcus = 256

	DataTypeUndefined = C._undefined
	DataTypeInt16     = C._16bit_integer
	DataTypeInt32     = C._32bit_integer
	DataTypeInt64     = C._64bit_integer
	DataTypeFloat32   = C._32bit_float
	DataTypeFloat64   = C._64bit_double
	DataTypeComplex32 = C._32bit_complex
	DataTypeUint32    = C._32bit_uint

	MinDataType = DataTypeInt16
	MaxDataType = DataTypeUint32
)

type ChannelIndex struct {
	DcuId         int
	ChannelNumber int
}

type ChannelAndIndex struct {
	Name string
	ChannelIndex
}

type channelAndIndexSort struct {
	channels []ChannelAndIndex
}

func (c *channelAndIndexSort) Len() int {
	return len(c.channels)
}

func (c *channelAndIndexSort) Less(i, j int) bool {
	return c.channels[i].Name < c.channels[j].Name
}

func (c *channelAndIndexSort) Swap(i, j int) {
	tmp := c.channels[i]
	c.channels[i] = c.channels[j]
	c.channels[j] = tmp
}

// OnlineChannel is the metadata describing a channel
type OnlineChannel struct {
	Name         string
	DcuId        int
	DataType     byte
	DataRate     int32
	BytesPer16th int32
	DataOffset   int32
	SignalGain   float32
	SignalSlope  float32
	SignalOffset float32
	Units        string
}

// Internal state while parsing the ini files
type parseData struct {
	channelLookup map[string]ChannelIndex
	dcuChannels   [MaxDcus][]OnlineChannel
	dcuChecksums  [MaxDcus]ligo_crc.LigoCrc32

	errMsg       string
	endingOffset int32
	currentDcuId int
}

func newParseData() *parseData {
	return &parseData{
		channelLookup: make(map[string]ChannelIndex, 0),
		currentDcuId:  -1,
	}
}

func (pd *parseData) resetDcu() {
	pd.currentDcuId = -1
	pd.endingOffset = 0
}

type ConfigFileInfo struct {
	Path      string
	Hash      uint32
	Timestamp time.Time
}

type DcuConfig struct {
	DcuId       int
	ChannelHash uint32
	ConfigHash  uint32
	Channels    []OnlineChannel
}

type IfoConfig struct {
	//Channels []ChannelAndIndex
	Files      []ConfigFileInfo `json:"-"`
	Dcus       map[int]DcuConfig
	ConfigHash uint32
}

func NewIfoConfig() *IfoConfig {
	return &IfoConfig{
		//Channels: make([]ChannelAndIndex, 0, 0),
		Files:      make([]ConfigFileInfo, 0, 0),
		Dcus:       make(map[int]DcuConfig, MaxDcus),
		ConfigHash: 0,
	}
}

/*
 * pointerMap holds pointers that are passed to cGo.  The pointer is saved
 * in the map, with a go non pointer object that can be passed to cGo.
 */
var pointerMap map[unsafe.Pointer]*parseData
var pointerMapLock sync.Mutex

func init() {
	pointerMap = make(map[unsafe.Pointer]*parseData)
}

func registerUserData(data *parseData) unsafe.Pointer {
	pointerMapLock.Lock()
	defer pointerMapLock.Unlock()

	p := unsafe.Pointer(new(byte))
	pointerMap[p] = data
	return p
}

func getUserData(index unsafe.Pointer) *parseData {
	pointerMapLock.Lock()
	defer pointerMapLock.Unlock()

	if data, ok := pointerMap[index]; !ok {
		panic("Bad index to the cgo user data map")
	} else {
		return data
	}
}

func releaseUserData(index unsafe.Pointer) {
	pointerMapLock.Lock()
	defer pointerMapLock.Unlock()

	delete(pointerMap, index)
}

func dataTypeSize(dataType byte) int32 {
	switch {
	case dataType == DataTypeInt16:
		return 2
	case dataType&(DataTypeInt32|DataTypeFloat32|DataTypeUint32) != 0:
		return 4
	case dataType&(DataTypeInt64|DataTypeFloat64|DataTypeComplex32) != 0:
		return 8
	}
	return 0
}

func hashChannel(h hash.Hash, channel *OnlineChannel) {
	h.Write([]byte(channel.Name))
	_ = binary.Write(h, binary.LittleEndian, &channel.DcuId)
	_ = binary.Write(h, binary.LittleEndian, &channel.DataType)
	_ = binary.Write(h, binary.LittleEndian, &channel.DataRate)
	_ = binary.Write(h, binary.LittleEndian, &channel.BytesPer16th)
	_ = binary.Write(h, binary.LittleEndian, &channel.DataOffset)
	_ = binary.Write(h, binary.LittleEndian, &channel.SignalGain)
	_ = binary.Write(h, binary.LittleEndian, &channel.SignalSlope)
	_ = binary.Write(h, binary.LittleEndian, &channel.SignalOffset)
	h.Write([]byte(channel.Units))
}

//export parseCb
func parseCb(name *C.char, params *C.CHAN_PARAM, user unsafe.Pointer) int {
	if name == nil || params == nil || user == nil {
		return 0
	}
	state := getUserData(user)
	chanName := C.GoString(name)
	if params.datatype < MinDataType || params.datatype > MaxDataType {
		state.errMsg += fmt.Sprintf("Invalid data type found on %s", chanName)
		return 0
	}
	if state.currentDcuId < 0 {
		state.currentDcuId = int(params.dcuid)
	}
	if state.currentDcuId != int(params.dcuid) {
		state.errMsg = fmt.Sprintf("Unexpected DCU change on %s", chanName)
		return 0
	}
	if state.currentDcuId < 0 || state.currentDcuId >= MaxDcus {
		state.errMsg += fmt.Sprintf("Invalid DCU on %s", chanName)
		return 0
	}
	if _, ok := state.channelLookup[chanName]; ok {
		state.errMsg += fmt.Sprintf("Duplicate channel %s", chanName)
		return 0
	}
	channel := OnlineChannel{
		Name:         chanName,
		DcuId:        state.currentDcuId,
		DataRate:     int32(params.datarate),
		DataType:     byte(params.datatype),
		BytesPer16th: dataTypeSize(byte(params.datatype)) * int32(params.datarate) / 16,
		DataOffset:   state.endingOffset,
		SignalGain:   float32(params.gain),
		SignalSlope:  float32(params.slope),
		SignalOffset: float32(params.offset),
		Units:        C.GoString(&params.units[0]),
	}
	hashChannel(&state.dcuChecksums[state.currentDcuId], &channel)

	state.channelLookup[chanName] = ChannelIndex{
		DcuId:         state.currentDcuId,
		ChannelNumber: len(state.dcuChannels[state.currentDcuId]),
	}
	state.dcuChannels[state.currentDcuId] = append(state.dcuChannels[state.currentDcuId], channel)
	state.endingOffset += channel.BytesPer16th

	return 1
}

func Parse(path string) (*IfoConfig, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	mainStat := FStat(f)
	if mainStat == nil {
		return nil, errors.New("unable to stat ini file")
	}

	state := newParseData()
	stateHandle := registerUserData(state)
	defer releaseUserData(stateHandle)

	ifo := NewIfoConfig()

	crcs := make(map[int]uint32)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := strings.TrimLeft(scanner.Text(), " \t")
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		if end := strings.IndexAny(line, " \t#"); end >= 0 {
			line = line[:end]
		}
		if strings.HasSuffix(line, ".par") {
			continue
		}

		err = func(line string, handle unsafe.Pointer) error {
			fileName := C.CString(line)
			defer C.free(unsafe.Pointer(fileName))
			var crc C.ulong
			C.parse_config_file(fileName, &crc, 0, handle)
			stat, err := os.Stat(line)
			if err != nil {
				return err
			}
			ifo.Files = append(ifo.Files, ConfigFileInfo{
				Path:      line,
				Hash:      uint32(crc),
				Timestamp: stat.ModTime(),
			})
			crcs[state.currentDcuId] = uint32(crc)
			return nil
		}(line, stateHandle)
		state.resetDcu()

	}
	if err = scanner.Err(); err != nil {
		return nil, err
	}

	for dcuId, dcuChannels := range state.dcuChannels {
		if len(dcuChannels) == 0 {
			continue
		}

		//for i := range dcuChannels {
		//	ifo.Channels = append(ifo.Channels, ChannelAndIndex{
		//		Name:         dcuChannels[i].Name,
		//		ChannelIndex: ChannelIndex{
		//			DcuId:         dcuId,
		//			ChannelNumber: i,
		//		},
		//	})
		//}
		ifo.Dcus[dcuId] = DcuConfig{
			DcuId:       dcuId,
			ChannelHash: state.dcuChecksums[dcuId].Sum32(),
			ConfigHash:  crcs[dcuId],
			Channels:    dcuChannels,
		}
	}
	ifo.Files = append(ifo.Files, ConfigFileInfo{
		Path:      mainStat.Name(),
		Hash:      0,
		Timestamp: mainStat.ModTime(),
	})

	//sorter := channelAndIndexSort{ channels: ifo.Channels }
	//sort.Sort(&sorter)

	return ifo, nil
}
