package parser

/*
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
*/
import "C"
import (
	"os"
	"time"
)

type fStatInfo struct {
	name string
	time time.Time
	size int64
}

func (f *fStatInfo) Name() string {
	return f.name
}

func (f *fStatInfo) Size() int64 {
	return f.size
}

func (f *fStatInfo) Mode() os.FileMode {
	return os.ModeIrregular
}

func (f *fStatInfo) ModTime() time.Time {
	return f.time
}

func (f *fStatInfo) IsDir() bool {
	return false
}

func (f *fStatInfo) Sys() interface{} {
	return nil
}

func FStat(f *os.File) os.FileInfo {
	if f == nil {
		return nil
	}
	var stat C.struct_stat
	rc := int(C.fstat(C.int(f.Fd()), &stat))
	if rc != 0 {
		return nil
	}
	return &fStatInfo{
		name: f.Name(),
		time: time.Unix(int64(stat.st_mtim.tv_sec), 0),
		size: int64(stat.st_size),
	}
}
